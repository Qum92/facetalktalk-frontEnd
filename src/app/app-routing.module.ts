import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FreeBoardComponent } from './layouts/free-board/free-board.component';
import { MyPageComponent } from './layouts/my-page/my-page.component';
import { SearchComponent } from './layouts/search/search.component';
import { SigninComponent } from './layouts/user/signin/signin.component';
import { HomeComponent } from './layouts/home/home.component';
import { BoardDetailComponent } from './layouts/free-board/board-detail/board-detail.component';
import { BoardWriteComponent } from './layouts/free-board/board-write/board-write.component';
import { ProductComponent } from './layouts/search/product/product.component';
import { ReviewComponent } from './layouts/search/review/review.component';
import { CustomerComponent } from './layouts/customer/customer.component';
import { SignComponent } from './layouts/user/signup/sign.component';
import { UserUpdateComponent } from './layouts/my-page/user-update/user-update.component';
import { UserAppUpdateComponent } from './layouts/my-page/user-app-update/user-app-update.component';
import { SearchnameComponent } from './layouts/home/searchname/searchname.component';
import { BoardEditComponent } from './layouts/free-board/board-edit/board-edit.component';
const routes: Routes = [
  {
    path:'',
    component:HomeComponent
  },
  {
    path:'freeBoard',
    component : FreeBoardComponent
  },
  {
    path:'myPage',
    component: MyPageComponent
  },
  {
    path:'search',
    component: SearchComponent
  },
  {
    path:'signin',
    component:SigninComponent
  },{
    path:'board-detail/:fbNum',
    component:BoardDetailComponent
  },
  {
    path:"board-write",
    component:BoardWriteComponent
  },
  {
    path:"board-edit/:fbNum",
    component:BoardEditComponent
  },
  {
    path:"product/:fpNum",
    component:ProductComponent
  },
  {
    path:"review",
    component:ReviewComponent
  },
  {
    path:"customer",
    component:CustomerComponent
  },
  {
    path:'signup',
    component:SignComponent
  },
  {
    path:'user-update',
    component:UserUpdateComponent
  },
  {
    path:'user-app-update',
    component:UserAppUpdateComponent
  },
  {
    path:'searchname',
    component:SearchnameComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
