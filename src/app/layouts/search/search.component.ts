import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductVo } from 'src/app/vo/product-vo';
import { CommonService } from 'src/app/common/common-service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Options, LabelType } from 'ng5-slider';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  styles: [`
  .star {
    position: relative;
    display: inline-block;
    font-size: 1rem;
    color: #d3d3d3;
  }
  .full {
    color: red;
  }
  .half {
    position: absolute;
    display: inline-block;
    overflow: hidden;
    color: red;
  }
`]
})

export class SearchComponent implements OnInit {
  // vr.1 서치페이지에서만 되는거
  // 1. menu comp 의 data 가 변경시 event 를 감지하는 event bindging 구현
  //   -> menu 의 변경시 자동으로 함수를 호출하게 하기 위함
  // 2. menu comp 의 변경 변수가 seahch 에 반영됨 확인 필요
  // 3. 반영된 변수를 findlist() 로 조회

  displayedColumns: string[] = ['fpFileName', 'fpName', 'fpVolume', 'fpPrice', 'itemReviewAvgscore', 'itemReviewCount'];
  dataSource = new MatTableDataSource<ProductVo>(); // 상품 테이블
  @ViewChild(MatPaginator) paginator: MatPaginator;  // 페이징
  productList: ProductVo[] = [];
  product: ProductVo = new ProductVo();

  // 가격 필터
  minPrice: number = 0;
  maxPrice: number = 500000;
  options: Options = {
    floor: 0,
    ceil: 500000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + '￦';
        case LabelType.High:
          return value + '￦';
        default:
          return '￦' + value;
      }
    }
  };

  constructor(
    private cs: CommonService,
    private router: Router,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit() {
    // this.product.fpCarte='립밤';
    // console.log('Called Constructor');
    this.route.queryParams.subscribe(
      params => {
        this.product.fpCarte = params['key'];
        this.findList(this.product);

      });
  }

  findList(product: ProductVo) {
    var url = `/select/category/${product.fpCarte}`;
    this.cs.get(url).subscribe(
      res => {
        this.productList = <ProductVo[]>res;
        this.dataSource = new MatTableDataSource<ProductVo>(this.productList);
        this.dataSource.paginator = this.paginator;  // 페이징
        // console.log(this.productList);
      })
  }
  // 가격 필터
  doFilter() {
    var url = `/select/category/${this.product.fpCarte}/${this.product.minPrice}/${this.product.maxPrice}`;
    this.cs.get(url).subscribe(
      res => {
        this.productList = <ProductVo[]>res;
        this.dataSource = new MatTableDataSource<ProductVo>(this.productList);
        this.dataSource.paginator = this.paginator;  // 페이징
        // console.log(res);
      },
      err => {
        console.log(err);
      }
    )
  }

  // change($event){                      vr.1 서치페이지에서만 되는거 
  //   this.product = <ProductVo>$event;
  //   this.findList(this.product);
  // }

  //테이블 필터
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  // 페이징 이동
  goPage(url: string) {
    this.router.navigate([url]);
  }

}
