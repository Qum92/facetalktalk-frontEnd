import { Component, OnInit } from '@angular/core';
import { ProductVo } from 'src/app/vo/product-vo';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/common/common-service';
import { NgbModal, ModalDismissReasons, NgbPaginationModule, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap'
import { IngredientVo } from 'src/app/vo/ingredient-vo';
import { ItemReviewVo } from 'src/app/vo/item-review-vo';
import { StorageService } from 'src/app/common/storage.service';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  styles: [`
  .star {
    position: relative;
    display: inline-block;
    font-size: 1rem;
    color: #d3d3d3;
  }
  .full {
    color: red;
  }
  .half {
    position: absolute;
    display: inline-block;
    overflow: hidden;
    color: red;
  }
`]
})
export class ProductComponent implements OnInit {
  fuTransState: Boolean = true;
  fpNum: number;
  product: ProductVo[] = [];

  ingredientList: IngredientVo[] = [];
  // item: ItemReviewVo;
  itemReviewList: ItemReviewVo[] = [];
  itemReview: ItemReviewVo = new ItemReviewVo();

  closeResult: string; //팝업 모달

  // 페이징
  currentPage = 1;
  itemsPerPage = 5;
  pageSize: number;
  // 리뷰 작성 버튼
  fuToken: boolean = true;
  // 성분정보 없는경우 안뜨게끔
  isingr : boolean =  false;
  constructor(
    private cs: CommonService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private _ss: StorageService,
  ) { }
  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.fpNum = params['fpNum'];
        this.getItemReview();
        this.productInfo();
      })

    // 리뷰 등록 여부 확인 용도  ( 리뷰미작성시에만 뜨도록 )
    this.selectReviewOverlap();

    // 성분정보 없는경우 안뜨게끔
    this.IngredientInfo();
    // 라디오버튼 기본 선택
    this.itemReview.fuTrans = null;
    this.itemReview.ageg = null;
    this.itemReview.fuSkinType = null;
    this.itemReview.fuSkinIssue = null;
  }
   // 리뷰 등록 여부 확인 용도  ( 리뷰미작성시에만 뜨도록 )
   selectReviewOverlap() {
    var param;
    this.route.params.subscribe(
      params => {
        param = params['fpNum'];
      })
    this.itemReview.fpNum = param;
    this.itemReview.fuId = this._ss.getSessionItem('fuId');
    var url=`/select/review/overlap/${this.itemReview.fuId}/${this.itemReview.fpNum}`;
    this.cs.get(url).subscribe(
      res=>{
        if(!res){
          if(this._ss.getSessionItem('fuToken')!=null){
             this.fuToken =false;
          }
        }
      },
      err=>{
        console.log(err);
      }
    )
  }
  // 성분정보
  IngredientInfo() {
    var url = "/ingredient/";
    this.cs.getByNum(url, this.fpNum).subscribe(
      res => {
        this.ingredientList = <IngredientVo[]>res;
        if(this.ingredientList.length!=0){  // 성분정보 없는경우 안뜨게끔
          this.isingr = true;
        }
      },
      err => {
        console.log(err);
      }
    )
  }
  // 상품정보
  productInfo() {
    var url = "/product/";
    this.cs.getByNum(url, this.fpNum).subscribe(
      res => {
        this.product = <ProductVo[]>res;
        // console.log(this.product);
      },
      err => {
        console.log(err);
      }
    )
  }
  // 상품후기 (리뷰) 보기
  getItemReview() {
    var url = "/review/select/";
    this.cs.getByNum(url, this.fpNum).subscribe(
      res => {
        this.itemReviewList = <ItemReviewVo[]>res;
        // console.log(this.itemReviewList);
      },
      error => {
        console.log(error);
      }
    )
  }
  // 상품 후기 쓰기 저장
  goSave() {
    var param;
    this.route.params.subscribe(
      params => {
        param = params['fpNum'];
      })
    var url = "/review/insert";
    this.itemReview.fpNum = param;
    this.itemReview.fuId = this._ss.getSessionItem('fuId');
    this.itemReview.fuAge = this._ss.getSessionItem('fuAge');
    this.itemReview.fuSkinIssue = this._ss.getSessionItem('fuSkinIssue');
    this.itemReview.fuSkinType = this._ss.getSessionItem('fuSkinType');
    this.itemReview.fuTrans = this._ss.getSessionItem('fuTrans');
    console.log(this.itemReview);
    this.cs.postFile(url, this.itemReview).subscribe(
      res => {
        if(res){
          window.location.reload();
          // console.log(this.itemReview);
        }
      },
      err => {
        console.log(err);
      }
    )
  }
  // 상품후기 테이블 페이징
  public onPageChange(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }
  public changePagesize(num: number): void {
    this.itemsPerPage = this.pageSize + num;
  }

  // 성분정보 팝업
  openLg(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.IngredientInfo();
      if(this.ingredientList.length==null){
        alert('없');
            }
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  // 상품 후기 쓰기 팝업 
  openVerticallyCentered(contentRev) {
    this.modalService.open(contentRev, { centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.goSave();  // 상품 후기 쓰기 저장
      window.location.reload();
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  // 상품후기 사진파일 올리기
  getFiles(evt) {
    this.itemReview.fpFile = evt.target.files[0];
    // console.log(this.itemReview.fpFile);
  }
  // ESC 누르면 나가기
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //   이전 페이지 이동
  goPage(pattern, param) {
    var option = {
      'queryParams': {
        'key': param
      }
    }
    this.router.navigate([pattern], option);
  }

  // 상품 후기(리뷰) 필터
  doFilter() {
    var param;
    this.route.params.subscribe(
      params => {
        param = params['fpNum'];
      })
    this.itemReview.fpNum = param;
    // console.log(this.itemReview.fpNum);
    // console.log(this.itemReview.fuTrans);
    // console.log(this.itemReview.ageg);
    // console.log(this.itemReview.fuSkinType);
    // console.log(this.itemReview.fuSkinIssue);
    // console.log(this.itemReview.itemReviewScore);

    var url = `/review/select/filter?`;
    for (var key in this.itemReview) {
      if(this.itemReview.itemReviewScore!=NaN){
        url += key + '=' + this.itemReview[key] + '&';
      }
    }

    this.cs.get(url).subscribe(
      res => {
        // console.log(res);
        this.itemReviewList = <ItemReviewVo[]>res;
      },
      err => {
        console.log(err);
      }
    )
  }
 

}
