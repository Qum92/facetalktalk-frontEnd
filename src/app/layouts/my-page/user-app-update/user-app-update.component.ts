import { Component, OnInit } from '@angular/core';
import { UserAddVO } from 'src/app/vo/userAdd-vo';
import { CommonService } from 'src/app/common/common-service';
import { StorageService } from 'src/app/common/storage.service';
import { Router } from '@angular/router';
import { UserVO } from 'src/app/vo/user-vo';

@Component({
  selector: 'app-user-app-update',
  templateUrl: './user-app-update.component.html',
  styleUrls: ['./user-app-update.component.scss']
})
export class UserAppUpdateComponent implements OnInit {
  userAdd : UserAddVO = new UserAddVO();
  userInfo : UserAddVO[]=[];

  constructor(
    private route:Router,
    private _ss: StorageService,
    private cs: CommonService
  ) { }

  ngOnInit() {
    this.selectUserAddInfo();
  }

  selectUserAddInfo(){
    this.userAdd.fuId=this._ss.getSessionItem('fuId');
    var url ="/user/add/";
    this.cs.getById(url, this.userAdd.fuId).subscribe(
      res=>{
        this.userInfo = <UserAddVO[]>res;
      },
      error=>{
        console.log(error);
      }
    )
  }

  UserAddInfoUpdate(){
    var url = "/useradd/update"
    this.cs.putJson(url, this.userInfo).subscribe(
      res=>{
        if(res){
          alert("수정완료");
          this.route.navigate(['myPage']);
        }
      },
      error=>{
        console.log(error);
      }
    )
  }

}
