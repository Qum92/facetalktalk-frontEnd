import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/common/storage.service';
import { CommonService } from 'src/app/common/common-service';
import { inquiryVO } from 'src/app/vo/inquiry-vo';
import { reportVO } from 'src/app/vo/report-vo';
import { ItemReviewVo } from 'src/app/vo/item-review-vo';
@Component({
  selector: 'app-my-page',
  templateUrl: './my-page.component.html',
  styleUrls: ['./my-page.component.scss']
})
export class MyPageComponent implements OnInit {
  inquiryList: inquiryVO[];
  inquiry: inquiryVO = new inquiryVO();
  reportList: reportVO[];
  report: reportVO = new reportVO();
  itemReviewList: ItemReviewVo[] = [];
  itemReview: ItemReviewVo = new ItemReviewVo();

  constructor(
    private route: Router,
    private _ss: StorageService,
    private cs: CommonService
  ) { }
  ngOnInit() {
    this.MyReviewList();
    this.inquirySelect();
    this.reportSelect();
  }
  goPage() {
    this.route.navigate(['/user-update']);
  }
  // 문의 내역
  inquirySelect() {
    this.inquiry.ciId = this._ss.getSessionItem('fuId');
    let url = `/inquiry/select/`;
    this.cs.getById(url, this.inquiry.ciId).subscribe(
      res => {
        if (res) {
          this.inquiryList = <inquiryVO[]>res;
          // console.log(this.inquiryList);
        }
      },
      err => {
        console.log(err);
      }
    )
  }
  // 신고 내역
  reportSelect() {
    this.report.crId = this._ss.getSessionItem('fuId');
    let url = `/report/select/`;
    this.cs.getById(url, this.report.crId).subscribe(
      res => {
        if (res) {
          this.reportList = <reportVO[]>res;
          // console.log(res);
        }
      },
      err => {
        console.log(err);
      }
    )
  }
  // 내가 쓴 후기들
  MyReviewList() {
    this.itemReview.fuId = this._ss.getSessionItem('fuId');
    let url = `/reviewList/`;
    this.cs.getById(url, this.itemReview.fuId).subscribe(
      res => {
        if (res) {
          this.itemReviewList = <ItemReviewVo[]>res;
          console.log(res);
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  // 페이징
  currentPage = 1;
  itemsPerPage = 5;
  pageSize: number;
  public onPageChange1(pageNum: number): void {
    this.pageSize = this.itemsPerPage * (pageNum - 1);
  }
  public changePagesize(num: number): void {
    this.itemsPerPage = this.pageSize + num;
  }

  // 페이징2
  currentPage2 = 1;
  itemsPerPage2 = 5;
  pageSize2 : number;
  // pageSize: number;
  public onPageChange2(pageNum2: number): void {
    this.pageSize2 = this.itemsPerPage2 * (pageNum2 - 1);
  }
  public changePagesize2(num: number): void {
    this.itemsPerPage2 = this.pageSize + num;
  }

  // 페이징3
  currentPage3 = 1;
  itemsPerPage3 = 5;
  pageSize3:number;
  // pageSize: number;
  public onPageChange3(pageNum3: number): void {
    this.pageSize3 = this.itemsPerPage3 * (pageNum3 - 1);
  }
  public changePagesize3(num3: number): void {
    this.itemsPerPage3 = this.pageSize + num3;
  }


}
