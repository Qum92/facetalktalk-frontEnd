import { Component, OnInit, ɵConsole } from '@angular/core';
import { Router } from '@angular/router';
import { UserVO } from 'src/app/vo/user-vo';
import { StorageService } from 'src/app/common/storage.service';
import { CommonService } from 'src/app/common/common-service';
import { LoginCheckVo } from 'src/app/vo/login-check-vo';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {
  user: LoginCheckVo = new LoginCheckVo();
  userUp : UserVO = new UserVO();
  userInfo : UserVO[]=[];
  
  isUserUpdate:boolean=true;
  isEmailUpdate:boolean=false;
  isNameUpdate:boolean=true;
  isPwdUpdate:boolean=true;
  isMobileUpdate:boolean=true;
  isAddrUpdate:boolean=true;

  fuEmailId:string;
  fuEmailAddr:string;
  fuPwdCon:string;

  constructor(
    private route:Router,
    private _ss: StorageService,
    private cs: CommonService
  ) { }

  ngOnInit() {
    this.user.fuId= this._ss.getSessionItem('fuId');
  }
  setDaumAddressApi(data) {
    this.userUp.fuZipcode = data.zip;
    this.userUp.fuAddr = data.addr;
  }
  
  doCheck(){
    this.cs.AjaxLogin(this.user).subscribe(res => {
      if (res.response) {
        this.isUserUpdate = false;
        this.selectUserInfo();
      } else {
        alert("비밀번호가 다릅니다.");
      }
    }, error => {
      console.log(error);
    })

  }

  selectUserInfo(){
    var url ="/user/";
    this.cs.getById(url, this.user.fuId).subscribe(
      res=>{
        this.userInfo = <UserVO[]>res;
      },
      error=>{
        console.log(error);
      }
    )
  }
  // 이메일 수정 버튼
  UpEmail(){
    this.isEmailUpdate=true;
  }
  UpName(){
    this.isNameUpdate=false;
  }
  UpPwd(){
    this.isPwdUpdate=false;
  }
  UpMobile(){
    this.isMobileUpdate=false;
  }
  UpAdd(){
    this.isAddrUpdate=false;
  }

  // 유저 정보 업데이트
  UserInfoUpdate(){
    if(this.fuEmailId!=null && this.fuEmailAddr!=null){
      this.userUp.fuEmail = this.fuEmailId + "@" + this.fuEmailAddr;
    }else if (this.userUp.fuPwd!=null){
      this.checkPwd();
    }else{   
    this.userUp.fuId=this._ss.getSessionItem('fuId');
    var url = "/signUp"
    this.cs.putJson(url, this.userUp).subscribe(
      res=>{
        if(res){
          alert("수정완료");
          this.route.navigate(['myPage']);
        }
      },
      error=>{
        console.log(error);
      }
    )
    }
  }

  checkPwd() {
    if(!this.userUp.fuPwd){
      alert("비밀번호를 입력해주세요.");
    }else if (this.userUp.fuPwd.length < 6) {
      alert("비밀번호는 6자리 이상입니다.");
    } else if (this.fuPwdCon != this.userUp.fuPwd) {
      alert("비밀번호를 확인해주세요.");
    }
  }

}
