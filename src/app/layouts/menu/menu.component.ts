import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatMenuTrigger } from '@angular/material';
import { ProductVo } from 'src/app/vo/product-vo';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/common/storage.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @Output() product: EventEmitter<ProductVo> = new EventEmitter();
  fuId: string;
  constructor(private _router: Router, private _ss: StorageService) { }
  ngOnInit() {
    this.fuId = this._ss.getSessionItem('fuId');
    // this.goMainPage('');
  }

  someMethod() {
    this.trigger.openMenu();
  }

  // getMenu(value){
  //   this.goPage('search', value);
  //   var product = new ProductVo();
  //   product.fpCarte=value;
  //   this.product.emit(product);   
  // }

  goPage(pattern, param) {
    if (param) {
      var option = {
        'queryParams': {
          'key': param
        }
      }
      this._router.navigate([pattern], option);
    }
  }
  goMainPage(url:string){
    this._router.navigate([url]);
  }
  logOut() {
    sessionStorage.clear();
    window.location.reload();
  }
  goMyPage(){
    if(!this.fuId){
      alert("로그인을 해야 사용 가능한 서비스 입니다.");
    }else{
      this.goMainPage('myPage');
    }
  }

}
