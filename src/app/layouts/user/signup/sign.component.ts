import { Component, OnInit } from '@angular/core';
import { UserVO } from 'src/app/vo/user-vo';
import { CommonService } from 'src/app/common/common-service';
import { UserAddVO } from 'src/app/vo/userAdd-vo';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.scss']
})
export class SignComponent implements OnInit {
  user : UserVO = new UserVO();
  userAdd : UserAddVO = new UserAddVO();
  fuEmailId:string;
  fuEmailAddr:string;
  fuPwdCon:string;
  constructor(private _http: CommonService, private _rt:Router) { }

  ngOnInit() {
    this.fuEmailAddr = "선택해주세요";
    this.userAdd.fuTrans = "선택해주세요";
    this.userAdd.fuChild = "선택해주세요";
    this.userAdd.fuSkinType = "선택해주세요";
    this.userAdd.fuSkinIssue = "선택해주세요";
  }
  //다음 주소 API
  setDaumAddressApi(data) {
    this.user.fuZipcode = data.zip;
    this.user.fuAddr = data.addr;
  }
  //넘어가기전에 알림창
  alam() {
    alert("데이터 손실을 막기 위해서 저장을 눌러주세요");
  }
  //아이디 Validation 및 중복확인
  checkId() {
    if (!this.user.fuId) {
      alert("아이디를 입력해주세요.");
    } else if (this.user.fuId.length < 5) {
      alert("아이디는 5글자 이상입니다.");
    } else {
      var url = "/signUp/";
      this._http.getById(url, this.user.fuId).subscribe(res => {
        if (res == 1) {
          alert("중복된 아이디 입니다.");
        } else {
          alert("사용가능한 아이디 입니다.");
        }
      })
    }
  }
  //비밀번호 Validation
  checkPwd() {
    if(!this.user.fuPwd){
      alert("비밀번호를 입력해주세요.");
    }else if (this.user.fuPwd.length < 6) {
      alert("비밀번호는 6자리 이상입니다.");
    } else if (this.fuPwdCon != this.user.fuPwd) {
      alert("비밀번호를 확인해주세요.");
    }
  }
  //필수정보 Validation 및 서버 저장
  signUp() {
    console.log(this.user);
    if (!this.user.fuId) {
      this.checkId();
    } else if (!this.user.fuPwd) {
      this.checkPwd();
    } else if (!this.user.fuName){
      alert("이름을 입력해주세요.");
    } else if (!this.user.fuAddr){
      alert("주소를 입력해주세요.")
    } else if (!this.user.fuMobile){
      alert("연락처를 입력해주세요.")
    } else if (!this.fuEmailId){
      alert("이메일을 입력해주세요.")
    }
    else {
      this.user.fuEmail = this.fuEmailId + "@" + this.fuEmailAddr;
      var url = "/signUp";
      this._http.postJson(url,this.user).subscribe(res=>{
        if(res==1){
          alert("저장되었습니다");
        }
      },error=>{
        console.log(error);
        alert("저장에 실패하였습니다");
      })
    }
  }
  //추가정보 서버 저장
  updateSignUp(){
    if(this.userAdd.fuTrans.length>1){
      alert("성별을 선택해주세요.")
    }else if(!this.userAdd.fuAge){
      alert("생년월일을 입력해주세요.")
    }else if(this.userAdd.fuSkinType.length>4){
      alert("피부타입을 선택해주세요.")
    }else if(this.userAdd.fuChild.length>1){
      alert("자녀여부를 선택해주세요.")
    }else if(this.userAdd.fuSkinIssue==='선택해주세요'){
      this.userAdd.fuSkinIssue='없음';
    }else{
    this.userAdd.fuId = this.user.fuId;
    var url = "/signUp"
    this._http.putJson(url,this.userAdd).subscribe(res=>{
      if(res==1){
        alert("저장되었습니다");
      }
    },error=>{
      console.log(error);
      alert("저장에 실패하였습니다.");
    })
    }
  }
  goPage(url:string){
    this._rt.navigate([url]);
  }
}