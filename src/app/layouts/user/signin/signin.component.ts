import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/common/common-service';
import { StorageService } from 'src/app/common/storage.service';
import { Router } from '@angular/router';
import { LoginVO } from 'src/app/vo/login-vo';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  fuIdState: Boolean = false;
  user: LoginVO = new LoginVO();
  constructor(private _http: CommonService, private _ss: StorageService, private _rt: Router) { }
  //로그인 하기 전에 아이디 기억상태 확인 및 설정저장
  ngOnInit() {
    var res = this._ss.getLocalItem('fuIdState');
    if (res === 'true') {
      this.fuIdState = true;
      this.user.fuId = this._ss.getLocalItem('fuId');
    }
  }
  goPage(url: string) {
    this._rt.navigate([url]);
  }
  //로그인 및 섹션스토리지에 아이디랑 토큰 값 저장
  doLogin() {
    console.log(this.user);
    var url = "/signin"
    this._http.postJson(url, this.user).subscribe(res => {
      if (res) {
        this.user = <LoginVO>res;

        if (this.fuIdState === true) {
          this._ss.setLocalItem('fuId', this.user.fuId);
          this._ss.setSessionItem('fuId', this.user.fuId);
          this._ss.setSessionItem('fuToken', this.user.fuToken);
          this._ss.setSessionItem('fuTrans', this.user.fuTrans);
          this._ss.setSessionItem('fuAge', this.user.fuAge);
          this._ss.setSessionItem('fuSkinType', this.user.fuSkinType);
          this._ss.setSessionItem('fuSkinIssue', this.user.fuSkinIssue);
          location.href = '';
        } else {
          localStorage.clear();
          this._ss.setSessionItem('fuId', this.user.fuId);
          this._ss.setSessionItem('fuToken', this.user.fuToken);
          this._ss.setSessionItem('fuTrans', this.user.fuTrans);
          this._ss.setSessionItem('fuAge', this.user.fuAge);
          this._ss.setSessionItem('fuSkinType', this.user.fuSkinType);
          this._ss.setSessionItem('fuSkinIssue', this.user.fuSkinIssue);
          location.href = '';
        }
      } else {
        alert("아이디와 비밀번호가 틀립니다.");
      }
    }, error => {
      console.log(error);
    })
  }
  //아이디 저장 상태 로컬스토리지에 저장
  getFuId() {
    this._ss.setLocalItem('fuIdState', this.fuIdState.toString());
  }
}
