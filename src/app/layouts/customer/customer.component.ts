import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/common/common-service';
import { inquiryVO } from 'src/app/vo/inquiry-vo';
import { reportVO } from 'src/app/vo/report-vo';
import { StorageService } from 'src/app/common/storage.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
  constructor(private _rt: Router, private modalService: NgbModal, private _http: CommonService, private _ss: StorageService) { }
  closeResult: string;
  inquiryCarteList: inquiryVO[];
  inquiry: inquiryVO = new inquiryVO();
  reportCarteList: reportVO[];
  report: reportVO = new reportVO();

  //페이지 이동시에 실행 될어야 할 로직들
  ngOnInit() {
    this.doSelectInquiry();
    this.doSeletReport();
  }
  //페이지 이동
  goPage(url: string) {
    this._rt.navigate([url]);
  }
  //문의 카테고리 서버에서 받는 것
  doSelectInquiry() {
    var url = "/inquiryCarteList"
    this._http.get(url).subscribe(res => {
      this.inquiryCarteList = <inquiryVO[]>res;
    })
  }
  //문의 팝업창
  openInquiry(inquiryModal) {
    if (!this._ss.getSessionItem('fuId')) {
      alert("로그인을 해야 사용 가능한 서비스 입니다.")
    } else {
      this.modalService.open(inquiryModal, { size: 'lg' }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        this.doInquirySave();

      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }
  //문의 팝업창에서 파일 받아 오는 것
  setInquiryFile(evt) {
    var reader = new FileReader();
    reader.onload = (e) => {
      this.inquiry.ciFileName = (<FileReader>e.target).result.toString();
    }
    reader.readAsDataURL(evt.target.files[0]);
    this.inquiry.ciFile = evt.target.files[0];
  }
  //문의 팝업창에서 받아온 정보를 서버쪽이랑 연결 및 결과
  doInquirySave() {
    this.inquiry.ciId = this._ss.getSessionItem('fuId');
    var url = "/inquiry";
    console.log(this.inquiry);
    this._http.postFile(url, this.inquiry).subscribe(res => {
      console.log(res);
      if (res == 1) {
        alert('문의작성을 하였습니다.');
      }
    }, error => {
      console.log(error);
    })
  }
  //신고 카테고리 서버에서 받는 것
  doSeletReport() {
    var url = "/reportCarteList"
    this._http.get(url).subscribe(res => {
      this.reportCarteList = <reportVO[]>res;
    })
  }
  //신고 팝업창
  openReport(reportModal) {
    if (!this._ss.getSessionItem('fuId')) {
      alert("로그인을 해야 사용 가능한 서비스 입니다.")
    } else {
      this.modalService.open(reportModal, { size: 'lg' }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        this.doReportSave();

      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }
  //신고 팝업창에서 파일 받아 오는 것
  setReportFile(evt) {
    var reader = new FileReader();
    reader.onload = (e) => {
      this.report.crFileName = (<FileReader>e.target).result.toString();
    }
    reader.readAsDataURL(evt.target.files[0]);
    this.report.crFile = evt.target.files[0];
  }
  //신고 팝업창에서 받아온 정보를 서버쪽이랑 연결 및 결과
  doReportSave() {
    this.report.crId = this._ss.getSessionItem('fuId');
    var url = "/report";
    console.log(this.report);
    this._http.postFile(url, this.report).subscribe(res => {
      console.log(res);
      if (res == 1) {
        alert('신고작성을 하였습니다.')
      }
    }, error => {
      console.log(error);
    })
  }
  //팝업창에서 닫기 공통으로 짜논 것
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}

