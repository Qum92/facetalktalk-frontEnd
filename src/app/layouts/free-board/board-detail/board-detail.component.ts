import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/common/common-service';
import { boardVO } from 'src/app/vo/board-vo';
import { MatTableDataSource } from '@angular/material';
import { commandVO } from 'src/app/vo/command-vo';
import { calcBindingFlags } from '@angular/core/src/view/util';
import { StorageService } from 'src/app/common/storage.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { reportVO } from 'src/app/vo/report-vo';

export interface PeriodicElement {

  fbTitle: string;
  fbId: string;
  fbCredat: string;
  fbCnt: number;
  fbLike: number;
  fbContent: string;
}
const ELEMENT_DATA: boardVO[] = [
];
@Component({
  selector: 'app-board-detail',
  templateUrl: './board-detail.component.html',
  styleUrls: ['./board-detail.component.scss']
})
export class BoardDetailComponent implements OnInit {
  displayedColumns: string[] = ['fbCarte', 'fbTitle', 'fbId', 'fbCnt', 'fbLike', 'fbCredat'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  fbNum: number;
  fb: boardVO;
  fbList: boardVO[] = [];
  cb: commandVO = new commandVO();
  cbList: commandVO[] = [];
  index: string = '명예회손, 개인정보 유출, 분쟁 유발, 허위 사실 유포 등의 글은 이용약관에 의해 제재는 물론 법률에 의해 처벌 받을 수 있습니다. 건전한 커뮤니티를 위해 자제를 당부 드립니다.';
  fuId: string;
  closeResult: string;
  reportCarteList: reportVO[];
  report: reportVO = new reportVO();
  constructor(private _rta: ActivatedRoute, private modalService: NgbModal, private _http: CommonService, private _rt: Router, private _ss: StorageService) { }

  ngOnInit() {
    this._rta.params.forEach(param => {
      this.fbNum = param['fbNum'];
    })
    this.refresh();
    this.selectCommand();
    this.fuId = this._ss.getSessionItem('fuId');
    this.doSeletReport();
  }
  refresh() {
    var url = "/board/";
    this._http.getByNum(url, this.fbNum).subscribe(res => {
      this.fbList = <boardVO[]>res;
      this.dataSource.data = this.fbList;
      for (let fb of this.fbList) {
        this.fb = fb;
        console.log(this.fb.fbContent);
      }
      document.querySelector('#content').innerHTML = this.fb.fbContent;
    })
  }
  like(): void {
    var url = "/boardup";
    var param = { 'fbNum': this.fb.fbNum, 'fbLike': this.fb.fbLike };
    this._http.postAjax(url, param).subscribe(res => {
      if (res.response) {
        console.log(res.response);
      }
    })
  }
  goPage(url: string) {
    this._rt.navigate([url]);
  }
  save(): void {
    this.cb.bdId = this._ss.getSessionItem('fuId');
    if (!this.cb.bdId) {
      alert("로그인을 해야 사용 가능한 서비스 입니다.");
    } else {
      this.cb.fbNum = this.fbNum;
      var url = "/command";
      var params = {
        'fbNum': this.cb.fbNum,
        'bdId': this.cb.bdId,
        'bdContent': this.cb.bdContent
      };
      console.log(params);
      this._http.postJson(url, params).subscribe(res => {
        if (res) {
          this.selectCommand();
        }
      })
    }
  }
  selectCommand() {
    var url = "/command/";
    this._http.getByNum(url, this.fbNum).subscribe(res => {
      if (res) {
        this.cbList = <commandVO[]>res;
      }
    })
  }
  edit() {
    var url = 'board-edit/'+this.fbNum
    this.goPage(url);
  }
  delete() {
    var url = '/board/'+this.fbNum
    this._http.deleteJson(url).subscribe(res=>{
      console.log(res);
    })
  }

  openReport(reportModal) {
    if (!this._ss.getSessionItem('fuId')) {
      alert("로그인을 해야 사용 가능한 서비스 입니다.")
    } else {
      this.modalService.open(reportModal, { size: 'lg' }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        this.doReportSave();

      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }
  //신고 팝업창에서 파일 받아 오는 것
  setReportFile(evt) {
    var reader = new FileReader();
    reader.onload = (e) => {
      this.report.crFileName = (<FileReader>e.target).result.toString();
    }
    reader.readAsDataURL(evt.target.files[0]);
    this.report.crFile = evt.target.files[0];
  }
  //신고 팝업창에서 받아온 정보를 서버쪽이랑 연결 및 결과
  doReportSave() {
    this.report.crId = this._ss.getSessionItem('fuId');
    var url = "/report";
    console.log(this.report);
    this._http.postFile(url, this.report).subscribe(res => {
      console.log(res);
      if (res == 1) {
        alert('신고작성을 하였습니다.')
      }
    }, error => {
      console.log(error);
    })
  }
  //신고 카테고리 서버에서 받는 것
  doSeletReport() {
    var url = "/reportCarteList"
    this._http.get(url).subscribe(res => {
      this.reportCarteList = <reportVO[]>res;
    })
  }
  //팝업창에서 닫기 공통으로 짜논 것
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
