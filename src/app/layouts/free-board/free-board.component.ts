import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, _MatTreeNodeMixinBase } from '@angular/material';
import { CommonService } from 'src/app/common/common-service';
import { boardVO } from 'src/app/vo/board-vo';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/common/storage.service';

export interface PeriodicElement {
  
  fbNum:number;
  fbTitle:string;
  fbId:string;
  fbCredat:string;
  fbCnt:number;
  fbLike:number;
}
const ELEMENT_DATA: boardVO[] = [
];
@Component({
  selector: 'app-free-board',
  templateUrl: './free-board.component.html',
  styleUrls: ['./free-board.component.scss']
})
export class FreeBoardComponent implements OnInit {
  displayedColumns: string[] = ['fbNum', 'fbCarte', 'fbTitle','fbId', 'fbCnt', 'fbLike','fbCredat'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private _http:CommonService,private _rt:Router,private _ss:StorageService) { }
  boardList:boardVO[];
  boardCarteList:boardVO[];
  board:boardVO = new boardVO;
  boardSelectList:boardVO[];
  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.doSelect();
    this.doGet();
    this.board.fbCarte='선택';
  }
  doSelect(){
    var url = "/boardList"
    this._http.get(url).subscribe(res=>{
      this.boardList = <boardVO[]>res;
      this.dataSource.data = this.boardList;
      for(let board of this.boardList){
        ELEMENT_DATA.push(board);
      }
    },error=>{
      console.log(error);
    })
  }
  doGet(){
    var url="/boardCarteList"
    this._http.get(url).subscribe(res=>{
      this.boardCarteList = <boardVO[]>res;
    })
  }
  doSearch(){
    var url="/board/"
    var fbCarte = this.board.fbCarte+"/"
    var fbTitle = this.board.fbTitle
    this._http.getByTitle(url,fbCarte,fbTitle).subscribe(res=>{
      this.boardSelectList = <boardVO[]>res;
      this.dataSource.data = this.boardSelectList;
      for(let board of this.boardSelectList){
        ELEMENT_DATA.push(board);
      }
    })
  }
  goPage(url:string){
    this._rt.navigate([url]);
  }
  writeBoard(){
    this.board.fbId = this._ss.getSessionItem('fuId');
    if(!this.board.fbId){
      alert("로그인을 해야 사용 가능한 서비스 입니다.")
    }else{
      this.goPage('board-write');
    }
  }
}
