import { Component, OnInit } from '@angular/core';
import { boardVO } from 'src/app/vo/board-vo';
import { StorageService } from 'src/app/common/storage.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/common/common-service';

@Component({
  selector: 'app-board-edit',
  templateUrl: './board-edit.component.html',
  styleUrls: ['./board-edit.component.scss']
})
export class BoardEditComponent implements OnInit {
  ckEditorConfig = {
    width:'1240px',
    height:'400px',
    filebrowserUploadUrl: 'http://localhost:89/upload/board/img',
    fileTools_requestHeaders: {
        'X-Requested-With': 'xhr',
        Authorization: 'Bearer ' + localStorage.getItem('access_token')
    },
    filebrowserUploadMethod: 'xhr',
    on: {
        instanceReady: function( evt ) {
            var editor = evt.editor;
            console.log('editor ===>', editor);
        },
        fileUploadRequest: function(evt) {
            console.log( 'evt ===>', evt );
        },
    },
  };
  public editorValue: string = '';
  boardCarteList:boardVO[];
  board:boardVO = new boardVO();
  oBoard:boardVO;
  boardList:boardVO[];
  fbNum:number;
  constructor(private _http:CommonService, private _rt:Router, private _ss:StorageService, private _rta: ActivatedRoute) { }

  ngOnInit() {
    this._rta.params.forEach(param => {
      this.fbNum = param['fbNum'];
    })
    this.doSelect();
    this.getBoardInfo();
  }
  doSelect(){
    var url ="/boardCarteList"
    this._http.get(url).subscribe(res=>{
      this.boardCarteList = <boardVO[]>res;
    })
  }
  getBoardInfo(){
    var url = "/board/"+this.fbNum;
    this._http.get(url).subscribe(res=>{
      this.boardList = <boardVO[]>res;
      for(let board of this.boardList){
        this.board.fbTitle = board.fbTitle;
        this.board.fbCarte = board.fbCarte;
        this.editorValue = board.fbContent;
      }    
    })
  }
  goPage(url:string){
    this._rt.navigate([url]);
  }
  doSave(){
    this.board.fbContent = this.editorValue;
    this.board.fbId = this._ss.getSessionItem('fuId');
    var url = "/boardup"
    var param = {'fbId': this.board.fbId,'fbContent':this.board.fbContent,
                 'fbTitle':this.board.fbTitle,'fbCarte':this.board.fbCarte,
                'fbNum':this.fbNum};
    this._http.postAjax(url,param).subscribe(res=>{
      console.log(res.response);
      this.goPage('freeBoard');
    })
  }
}
