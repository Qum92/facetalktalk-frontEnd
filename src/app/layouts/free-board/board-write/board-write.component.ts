import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/common/common-service';
import { Router } from '@angular/router';
import { boardVO } from 'src/app/vo/board-vo';
import { StorageService } from 'src/app/common/storage.service';

@Component({
  selector: 'app-board-write',
  templateUrl: './board-write.component.html',
  styleUrls: ['./board-write.component.scss']
})
export class BoardWriteComponent implements OnInit {
  ckEditorConfig = {
    width:'1240px',
    height:'400px',
    filebrowserUploadUrl: 'http://localhost:89/upload/board/img',
    fileTools_requestHeaders: {
        'X-Requested-With': 'xhr',
        Authorization: 'Bearer ' + localStorage.getItem('access_token')
    },
    filebrowserUploadMethod: 'xhr',
    on: {
        instanceReady: function( evt ) {
            var editor = evt.editor;
            console.log('editor ===>', editor);
        },
        fileUploadRequest: function(evt) {
            console.log( 'evt ===>', evt );
        },
    },
  };
  public editorValue: string = '';
  boardCarteList:boardVO[];
  board:boardVO = new boardVO();
  constructor(private _http:CommonService, private _rt:Router, private _ss:StorageService) { }

  ngOnInit() {
    this.doSelect();
    this.board.fbCarte='선택해주세요';
  }
  doSelect(){
    var url ="/boardCarteList"
    this._http.get(url).subscribe(res=>{
      this.boardCarteList = <boardVO[]>res;
      console.log(this.boardCarteList);
    })
  }
  goPage(url:string){
    this._rt.navigate([url]);
  }
  doSave(){
    this.board.fbContent = this.editorValue;
    this.board.fbId = this._ss.getSessionItem('fuId');
    var url = "/board"
    var param = {'fbId': this.board.fbId,'fbContent':this.board.fbContent,
                 'fbTitle':this.board.fbTitle,'fbCarte':this.board.fbCarte};
    this._http.postAjax(url,param).subscribe(res=>{
      if(res.response==1){
        this.goPage('freeBoard');
      }else{
        console.log(res.response);
      }
    })
  }
}
