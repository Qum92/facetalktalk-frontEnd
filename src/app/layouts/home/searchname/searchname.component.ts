import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/common/common-service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductVo } from 'src/app/vo/product-vo';

@Component({
  selector: 'app-searchname',
  templateUrl: './searchname.component.html',
  styleUrls: ['./searchname.component.scss'],
  styles: [`
  .star {
    position: relative;
    display: inline-block;
    font-size: 1rem;
    color: #d3d3d3;
  }
  .full {
    color: red;
  }
  .half {
    position: absolute;
    display: inline-block;
    overflow: hidden;
    color: red;
  }
`]
})
export class SearchnameComponent implements OnInit {
  productList: ProductVo[] = [];
  product: ProductVo = new ProductVo();

  isValue:boolean = true;

  constructor(
    private cs: CommonService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getText();
    this.searchList();
     
  }
  getText(){
    this.route.queryParams.subscribe(
      params=>{
        this.product.fpName=params['key'];
      }
    )
  }
  searchList(){
    let url =  `/fpname/search/${this.product.fpName}`;
    this.cs.get(url).subscribe(
      res=>{
        this.productList=<ProductVo[]>res;
        if(this.productList.length==0){
          this.isValue=false;
        }
      }
    )
  }

  // 페이징 이동
  goPage(url: string) {
    this.router.navigate([url]);
  }



}
