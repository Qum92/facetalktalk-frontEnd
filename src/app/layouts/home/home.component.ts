import { Component, OnInit, ViewChild, NgZone, EventEmitter, Output } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ProductVo } from 'src/app/vo/product-vo';
export interface Category{
  value : string;
  viewValue:string;
  twoCategory:string[];
  visiblity:boolean;
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [NgbCarouselConfig]
})

export class HomeComponent implements OnInit {
  view:boolean=false;
  imgSrcs1 = [];   
  imgSrcs2 = [];  
  product: ProductVo= new ProductVo();
  constructor(
    private router:Router
    ) {
  }

  goPage(pattern, param){
    if (param) {
      var option = {
        'queryParams': {
          'key': param
        }
      }
      this.router.navigate([pattern], option);
    }
  }

  ngOnInit() {
    for(var i=1;i<6;i++){
      this.imgSrcs1.push('assets/image/co' + i + '.jpg');
    } 
    for(var j=1;j<6;j++){
      this.imgSrcs2.push('assets/image/so' + j + '.jpg');
    } 
  }
  viewCategory(){
    if(this.view==false){
      this.view = true;
    }else{
      this.view = false;
    }
  }
  // 검색 버튼
  search(){
  }

}
