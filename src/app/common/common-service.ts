import { Injectable } from '@angular/core';
import { ajax } from 'rxjs/ajax';
import { HttpHeaders, HttpClient} from '@angular/common/http';
import { LoginCheckVo } from '../vo/login-check-vo';
const baseUrl = "http://localhost:89";
const httpJson = {
  headers: new HttpHeaders(
    {'Content-Type':'application/json'}
  )
}
const httpFile = {
  headers: new HttpHeaders(
    {'ENCTYPE':'multipart/form-data'}
  )
}
const ajaxJson = {
  headers: new HttpHeaders(
    {'Content-Type':'application/json','rxjs-custom-header':'Rxjs'}
  )
}

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  private baseUrl:string="http://localhost:89/";


  constructor(private _http:HttpClient) { }
get(url){
  url = baseUrl + url;
  console.log(url);
  return this._http.get(url);
}
getByNum(url,params){
  url = baseUrl + url;
  return this._http.get(url+params);
}
getByTitle(url,carte,title){
  url = baseUrl + url;
  return this._http.get(url+carte+title);
}
getById(url,param){
  url = baseUrl + url;
  return this._http.get(url+param);
}
postJson(url,param){
  url = baseUrl + url;
  return this._http.post(url,param,httpJson);
}
putJson(url,param){
  url = baseUrl + url;
  return this._http.put(url,param,httpJson);
}
postFile(url,param){
  url = baseUrl + url;
  let formData:FormData = new FormData();
  for(let key in param){
    formData.append(key, param[key]);
  }
  return this._http.post(url,formData,httpFile);
}
deleteFile(url,param){
  url = baseUrl + url;
  let formData:FormData = new FormData();
  console.log(param);
  for(let key in param){
    formData.delete(key);
  }
  return this._http.delete(url);
}
putAjax(url,params){
  url = baseUrl + url;
  return ajax.put(url,params,ajaxJson);
}
postAjax(url,params){
  url = baseUrl + url;
  return ajax.post(url,params,ajaxJson);
}
deleteJson(url){
  url = baseUrl + url;
  return this._http.delete(url,httpJson);
}

AjaxLogin(user:LoginCheckVo){
  return ajax.post(this.baseUrl+'signin',
  {'fuId':user.fuId,'fuPwd':user.fuPwd, },
  {'Content-Type':'application/json', 
  'rxjs-custom-header':'Rxjs'}
  );
}


}