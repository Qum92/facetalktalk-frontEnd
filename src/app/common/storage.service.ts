import { Injectable } from '@angular/core';
import { UserVO } from '../vo/user-vo';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  getLocalItem(key:string){
    return localStorage.getItem(key);
  }

  setLocalItem(key:string,value:string){
    localStorage.setItem(key,value);
  }

  getSessionItem(key:string){
    return sessionStorage.getItem(key);
  }

  setSessionItem(key:string,value:string){
    sessionStorage.setItem(key,value);
  }
}
