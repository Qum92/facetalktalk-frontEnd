export class ProductVo {
    fpNum:number;
	fpName:string;
	fpVolume:string;
	fpPrice:number;
	fpCarte:string;
	
	fpSeller:string;
	fpDetail:string;
	fpFileName:string;

	itemReviewCount: number;
	itemReviewAvgscore:number;
	// 가격조회 필터용 가상컬럼
	minPrice: number;
	maxPrice: number;

}
