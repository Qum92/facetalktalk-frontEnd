export class UserVO{
        fuId:string;
        fuIdState:boolean;
        fuName:string;
        fuPwd:string;
        fuToken:string;
        fuZipcode:string;
        fuAddr:string;
        fuAddrDetail:string;
        fuMobile:string;
        fuEmail:string;
}