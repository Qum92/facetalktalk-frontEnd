export class IngredientVo {
    fiNum:string;
    fiSafeLevel:string;
    fiKorName:string;
    fiEngName:string;
    fiDetail:string;
    fpNum:number;
}
