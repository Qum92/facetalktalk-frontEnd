export class ItemReviewVo {

	itemReviewNum : number ; 
	fuId : string ; 
	itemReviewScore : number ; 
	itemReviewContent : string ; 
	itemReviewReport : number ; 
	itemReviewDate : string ; 
	itemReviewTime : string ; 
	itemReviewUpdatedDate : string ; 
	itemReviewUpdatedTime : string ; 
	itemReviewPath1 : string ; 
	itemReviewPath2 : string ; 

	fpFile:File;

	fuName : string;
	fuTrans : string;
	fuAge: string;
	fuSkinType: string;
	fuSkinIssue:string;
	ageg:string;

	fpNum : number ; 


}
