import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './layouts/home/home.component';
import { MenuComponent } from './layouts/menu/menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule, MatInputModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { FreeBoardComponent } from './layouts/free-board/free-board.component';
import { MyPageComponent } from './layouts/my-page/my-page.component';
import { SearchComponent } from './layouts/search/search.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { BoardDetailComponent } from './layouts/free-board/board-detail/board-detail.component';
import { NgbCarouselModule, NgbAlertModule, NgbActiveModal, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDividerModule } from '@angular/material/divider';
import { MatTreeModule } from '@angular/material/tree';
import { BoardWriteComponent } from './layouts/free-board/board-write/board-write.component';
import { ProductComponent } from './layouts/search/product/product.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReviewComponent } from './layouts/search/review/review.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { CustomerComponent } from './layouts/customer/customer.component';
import { NgDaumAddressModule } from 'ng2-daum-address';
import { CdkStepperModule } from '@angular/cdk/stepper';
import {MatSliderModule} from '@angular/material/slider';
import { Ng5SliderModule } from 'ng5-slider';
import { SigninComponent } from './layouts/user/signin/signin.component';
import { SignComponent } from './layouts/user/signup/sign.component';
import { UserUpdateComponent } from './layouts/my-page/user-update/user-update.component';
import { FooterComponent } from './footer/footer.component';
import { UserAppUpdateComponent } from './layouts/my-page/user-app-update/user-app-update.component';
import { CKEditorModule } from 'ckeditor4-angular';
import { SearchnameComponent } from './layouts/home/searchname/searchname.component';
import { BoardEditComponent } from './layouts/free-board/board-edit/board-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,
    FreeBoardComponent,
    MyPageComponent,
    SearchComponent,
    BoardDetailComponent,
    BoardWriteComponent,   
    ProductComponent,
    ReviewComponent,
    CustomerComponent,
    SigninComponent,
    SignComponent,
    UserUpdateComponent,
    FooterComponent,
    UserAppUpdateComponent,
    SearchnameComponent,
    BoardEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatMenuModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatStepperModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    HttpClientModule,
    FormsModule,
    MatGridListModule,
    MatCarouselModule,
    NgbCarouselModule,    
    MatListModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatTreeModule,
    NgbModule,
    NgbPaginationModule,
    MatExpansionModule,
    NgDaumAddressModule,
    MatSliderModule,
    CdkStepperModule,
    ReactiveFormsModule,
    Ng5SliderModule,
    CKEditorModule
    
  ],
  providers: [],
  bootstrap: [AppComponent,MenuComponent]
})
export class AppModule { }